<?php

/*
 * This file is part of the drosalys-web/string-extensions package.
 *
 * (c) Benjamin Georgeault <https://www.drosalys-web.fr/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\StringExtensions\Canonicalizer;

/**
 * Class Canonicalizer
 *
 * @author Benjamin Georgeault
 */
class Canonicalizer implements CanonicalizerInterface
{
    /**
     * @inheritDoc
     */
    public function canonicalize(string $value): string
    {
        $encoding = mb_detect_encoding($value);

        return $encoding
            ? mb_convert_case($value, MB_CASE_LOWER, $encoding)
            : mb_convert_case($value, MB_CASE_LOWER)
        ;
    }
}
