<?php

/*
 * This file is part of the drosalys-web/string-extensions package.
 *
 * (c) Benjamin Georgeault <https://www.drosalys-web.fr/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\StringExtensions\Random;

use Symfony\Component\Uid\Uuid;

/**
 * Class UuidTokenGenerator
 *
 * @author Benjamin Georgeault
 */
class UuidTokenGenerator implements TokenGeneratorInterface
{
    /**
     * @inheritDoc
     */
    public function generateToken(): string
    {
        return Uuid::v4()->toRfc4122();
    }
}
