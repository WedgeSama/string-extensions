<?php

/*
 * This file is part of the drosalys-web/string-extensions package.
 *
 * (c) Benjamin Georgeault <https://www.drosalys-web.fr/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\StringExtensions\Bridge\Symfony;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class DrosalysWebStringExtensionsBundle
 *
 * @author Benjamin Georgeault
 */
class DrosalysWebStringExtensionsBundle extends Bundle
{
}
