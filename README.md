String Extensions
=================

A library to extend, manipulate and normalize string.

## Install

```
composer require drosalys-web/string-extensions
```

## Documentations

Here all capabilities documentations:
- [Canonicalizer](doc/canonicalizer.md): Process string and text to be clean for searching and indexing.

## TODO

- Better documentation.

## License

This bundle is under the MIT license. See the complete license:

    LICENSE
